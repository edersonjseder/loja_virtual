package com.lojavirtual.constants;

public class ContaPagarConstants {
    public static final String FORNECEDOR_NAO_ENCONTRADO_MESSAGE = "Fornecedor nao encontrado";
    public static final String PESSOA_NAO_ENCONTRADA_MESSAGE = "Pessoa nao encontrada";
    public static final String EMPRESA_NAO_ENCONTRADA_MESSAGE = "Empresa nao encontrada";
    public static final String CONTA_PAGAR_EXISTENTE_MESSAGE = "Ja existe conta a pagar com a mesma descricao";
    public static final String CONTA_PAGAR_NAO_ENCONTRADO_MESSAGE = "Conta nao encontrada";
}
