package com.lojavirtual.constants;

public class PessoaMensagemConstants {
    public static final String CPF_MENSAGEM = "Números inválidos do CPF: ";
    public static final String CNPJ_MENSAGEM = "Números inválidos do CNPJ: ";
    public static final String TIPO_PESSOA_MENSAGEM = "Informe o tipo Jurídico ou Fornecedor da Loja";
    public static final String CPF_EXISTENTE_MENSAGEM = "Já existe CPF cadastrado com o número ";
    public static final String CNPJ_EXISTENTE_MENSAGEM = "Já existe CNPJ cadastrado com o número ";
    public static final String IE_EXISTENTE_MENSAGEM = "Já existe IE cadastrado com o número ";
    public static final String IM_EXISTENTE_MENSAGEM = "Já existe IM cadastrado com o número ";
}
