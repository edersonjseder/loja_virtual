package com.lojavirtual.exception;

public class PessoaException extends RuntimeException {
    public PessoaException(String value) {
        super(value);
    }
}
