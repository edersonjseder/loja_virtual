package com.lojavirtual.exception;

public class ProdutoException extends RuntimeException {
    public ProdutoException(String value) {
        super(value);
    }
}
