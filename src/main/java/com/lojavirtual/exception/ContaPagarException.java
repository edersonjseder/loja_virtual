package com.lojavirtual.exception;

public class ContaPagarException extends RuntimeException {
    public ContaPagarException(String value) {
        super(value);
    }
}
