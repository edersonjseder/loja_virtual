package com.lojavirtual.exception;

public class MarcaProdutoException extends RuntimeException {
    public MarcaProdutoException(String value) {
        super(value);
    }
}
