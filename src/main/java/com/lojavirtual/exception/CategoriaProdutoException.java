package com.lojavirtual.exception;

public class CategoriaProdutoException extends RuntimeException {
    public CategoriaProdutoException(String value) {
        super(value);
    }
}
