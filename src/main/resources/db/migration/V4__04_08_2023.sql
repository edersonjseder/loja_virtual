ALTER TABLE avaliacao_produto ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE categoria_produto ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE conta_pagar ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE conta_receber ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE cupom_desconto ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE endereco ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE forma_pagamento ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE imagem_produto ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE item_venda_loja ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE marca_produto ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE nota_fiscal_compra ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE nota_fiscal_venda ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE nota_item_produto ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE pessoa_fisica ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE pessoa_juridica ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE produto ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE status_rastreio ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE usuario ALTER COLUMN empresa_id DROP NOT NULL;
ALTER TABLE vd_compra_loja ALTER COLUMN empresa_id DROP NOT NULL;